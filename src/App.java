import com.devcamp.jbr1s40.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
      
        InvoiceItem invoiceItem1 = new InvoiceItem("AD1235C", "Kem danh rang", 12, 25000.500);
        InvoiceItem invoiceItem2 = new InvoiceItem("EC456C", "kem chong nang", 6, 105000.700);

        System.out.println(invoiceItem1);
        System.out.println(invoiceItem2);
    }
}
